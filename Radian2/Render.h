#pragma once
#include "Color.h"

namespace Render
{

	void Text(int x, int y, Color col, DWORD font, const char* text)
	{

		int r = col.red;
		int g = col.green;
		int b = col.blue;
		int a = col.alpha;

		size_t origsize = strlen(text) + 1;
		const size_t newsize = 100;
		size_t convertedChars = 0;
		wchar_t wcstring[newsize];
		mbstowcs_s(&convertedChars, wcstring, origsize, text, _TRUNCATE);

		g_pSurface->DrawSetTextFont(font);
		g_pSurface->DrawSetTextColor(r, g, b, a);
		g_pSurface->DrawSetTextPos(x, y);
		g_pSurface->DrawPrintText(wcstring, wcslen(wcstring));
		return;
	}

}