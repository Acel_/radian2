#include "Aimbot.h"
//#include "Autowall.h"
#include "LagComp.h"
//#include "..\AntiAim\AntiAim.h"
#include "..\..\Utils\Utils.h"
#include "..\..\SDK\IVEngineClient.h"
#include "..\..\SDK\PlayerInfo.h"
#include "..\..\SDK\ICvar.h"
#include "..\..\Utils\Math.h"
#include "..\..\SDK\Hitboxes.h"
#include "..\..\Menu\Menu.h"

Aimbot g_Aimbot;


//bool Aimbot::HitChance(C_BaseEntity* pEnt, C_BaseCombatWeapon* pWeapon, Vector Angle, Vector Point, int chance)
//{
//	if (chance == 0 || g_Menu.Config.Hitchance == 0)
//		return true;
//
//	if (Backtrack[pEnt->EntIndex()] || ShotBacktrack[pEnt->EntIndex()]) // doing this bec im lazy
//	{
//		float Velocity = g::pLocalEntity->GetVelocity().Length();
//
//		if (Velocity <= (g::pLocalEntity->GetActiveWeapon()->GetCSWpnData()->max_speed_alt * .34f))
//			Velocity = 0.0f;
//
//		float SpreadCone = pWeapon->GetAccuracyPenalty() * 256.0f / M_PI + pWeapon->GetCSWpnData()->max_speed * Velocity / 3000.0f; // kmeth https://github.com/DankPaster/kmethdude
//		float a = (Point - g::pLocalEntity->GetEyePosition()).Length();
//		float b = sqrt(tan(SpreadCone * M_PI / 180.0f) * a);
//		if (2.2f > b) return true;
//		return (chance <= ((2.2f / fmax(b, 2.2f)) * 100.0f));
//	}
//
//	float Seeds = (g_Menu.Config.Hitchance == 1) ? 356.f : 256.f;
//
//	Angle -= (g::pLocalEntity->GetAimPunchAngle() * g_pCvar->FindVar("weapon_recoil_scale")->GetFloat());
//
//	Vector forward, right, up;
//
//	g_Math.AngleVectors(Angle, &forward, &right, &up);
//
//	int Hits = 0, neededHits = (Seeds * (chance / 100.f));
//
//	float weapSpread = pWeapon->GetSpread(), weapInaccuracy = pWeapon->GetInaccuracy();
//
//	for (int i = 0; i < Seeds; i++)
//	{
//		float Inaccuracy = g_Math.RandomFloat(0.f, 1.f) * weapInaccuracy;
//		float Spread = g_Math.RandomFloat(0.f, 1.f) * weapSpread;
//
//		Vector spreadView((cos(g_Math.RandomFloat(0.f, 2.f * M_PI)) * Inaccuracy) + (cos(g_Math.RandomFloat(0.f, 2.f * M_PI)) * Spread), (sin(g_Math.RandomFloat(0.f, 2.f * M_PI)) * Inaccuracy) + (sin(g_Math.RandomFloat(0.f, 2.f * M_PI)) * Spread), 0), direction;
//		direction = Vector(forward.x + (spreadView.x * right.x) + (spreadView.y * up.x), forward.y + (spreadView.x * right.y) + (spreadView.y * up.y), forward.z + (spreadView.x * right.z) + (spreadView.y * up.z)).Normalize();
//
//		Vector viewanglesSpread, viewForward;
//
//		g_Math.VectorAngles(direction, up, viewanglesSpread);
//		g_Math.NormalizeAngles(viewanglesSpread);
//
//		g_Math.AngleVectors(viewanglesSpread, &viewForward);
//		viewForward.NormalizeInPlace();
//
//		viewForward = g::pLocalEntity->GetEyePosition() + (viewForward * pWeapon->GetCSWpnData()->range);
//
//		C_Trace Trace;
//
//		g_pTrace->ClipRayToEntity(C_Ray(g::pLocalEntity->GetEyePosition(), viewForward), mask_shot | contents_grate, pEnt, &Trace);
//
//		if (Trace.m_pEnt == pEnt)
//			Hits++;
//
//		if (((Hits / Seeds) * 100.f) >= chance)
//			return true;
//
//		if ((Seeds - i + Hits) < neededHits)
//			return false;
//	}
//
//	return false;
//}


void Aimbot::OnCreateMove()
{
	if (!g_pEngine->IsInGame())
		return;

	Vector Aimpoint = { 0,0,0 };
	C_BaseEntity* Target = nullptr;

	int targetID = 0;
	int tempDmg = 0;
	static bool shot = false;

	for (int i = 1; i <= g_pEngine->GetMaxClients(); ++i)
	{
		C_BaseEntity* pPlayerEntity = g_pEntityList->GetClientEntity(i);

		if (!pPlayerEntity
			|| !pPlayerEntity->IsAlive()
			|| pPlayerEntity->IsDormant())
		{
			g_LagComp.ClearRecords(i);
			continue;
		}

		g_LagComp.StoreRecord(pPlayerEntity);

		if (pPlayerEntity == g::pLocalEntity || pPlayerEntity->GetTeam() == g::pLocalEntity->GetTeam())
			continue;

		g::EnemyEyeAngs[i] = pPlayerEntity->GetEyeAngles();

		if (g_LagComp.PlayerRecord[i].size() == 0 || !g::pLocalEntity->IsAlive() || !g_Menu.Config.Aimbot || g_Menu.Config.LegitBacktrack == 0)
			continue;

		if (!g::pLocalEntity->GetActiveWeapon() || g::pLocalEntity->IsKnifeorNade())
			continue;

		bestEntDmg = 0;

		/*Vector Hitbox = Hitscan(pPlayerEntity);

		if (Hitbox != Vector(0, 0, 0) && tempDmg <= bestEntDmg)
		{
			Aimpoint = Hitbox;
			Target = pPlayerEntity;
			targetID = Target->EntIndex();
			tempDmg = bestEntDmg;
		}*/
	}

	if (!g::pLocalEntity->IsAlive())
	{
		shot = false;
		return;
	}

	if (!g::pLocalEntity->GetActiveWeapon() || g::pLocalEntity->IsKnifeorNade())
	{
		shot = false;
		return;
	}

	if (shot)
	{
		if (g_Menu.Config.FixShotPitch) // ik it dosnt realy fix much just makes ur pitch go down faster
		{
			g::bSendPacket = true;
			//g_AntiAim.OnCreateMove();
		}
		shot = false;
	}

	float flServerTime = g::pLocalEntity->GetTickBase() * g_pGlobalVars->intervalPerTick;
	bool canShoot = (g::pLocalEntity->GetActiveWeapon()->GetNextPrimaryAttack() <= flServerTime);

	if (Target)
	{
		g::TargetIndex = targetID;

		float SimulationTime = 0.f;

		if (Backtrack[targetID])
			SimulationTime = g_LagComp.PlayerRecord[targetID].at(0).SimTime;
		else
			SimulationTime = g_LagComp.PlayerRecord[targetID].at(g_LagComp.PlayerRecord[targetID].size() - 1).SimTime;

		if (ShotBacktrack[targetID])
			SimulationTime = g_LagComp.PlayerRecord[targetID].at(g_LagComp.ShotTick[targetID]).SimTime;

		Vector Angle = g_Math.CalcAngle(g::pLocalEntity->GetEyePosition(), Aimpoint);

		/*if (g::pLocalEntity->GetVelocity().Length() >= (g::pLocalEntity->GetActiveWeapon()->GetCSWpnData()->max_speed_alt * .34f) - 5 && !GetAsyncKeyState(VK_SPACE))
			Autostop();*/

		/*if (!(g::pCmd->buttons & IN_ATTACK) && canShoot && HitChance(Target, g::pLocalEntity->GetActiveWeapon(), Angle, Aimpoint, g_Menu.Config.HitchanceValue))
		{

			if (!Backtrack[targetID] && !ShotBacktrack[targetID])
				g::Shot[targetID] = true;

			if (g_Menu.Config.Ak47meme)
				g_pEngine->ExecuteClientCmd("play weapons\\ak47\\ak47-1.wav");

			g::bSendPacket = true;
			shot = true;

			g::pCmd->viewangles = Angle - (g::pLocalEntity->GetAimPunchAngle() * g_pCvar->FindVar("weapon_recoil_scale")->GetFloat());
			g::pCmd->buttons |= IN_ATTACK;
			g::pCmd->tick_count = TIME_TO_TICKS(SimulationTime + g_LagComp.LerpTime());
		}*/
	}
}