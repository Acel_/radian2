﻿#include <thread>
#include "Hooks.h"
#include "Utils\Utils.h"
#include "Utils\GlobalVars.h"

HINSTANCE HThisModule;

int OnDllAttach()
{
    Hooks::Init();

	while (true)
		Sleep(10000000);

	Hooks::Restore();

	FreeLibraryAndExitThread(HThisModule, 0);
}

void SetupConsole()
{
	//Allocate a console and make sure I can write to it
	AllocConsole();
	freopen("CONOUT$", "wb", stdout);
	freopen("CONOUT$", "wb", stderr);
	freopen("CONIN$", "rb", stdin);
	SetConsoleTitle("CSGOSimpleInternal");
}

BOOL WINAPI DllMain(HMODULE hModule, DWORD dwReason, LPVOID lpReserved)
{
    if (dwReason == DLL_PROCESS_ATTACH && GetModuleHandleA("csgo.exe"))
    {
		//SetupConsole();
        CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)OnDllAttach, NULL, NULL, NULL);
    }

    return TRUE;
}

