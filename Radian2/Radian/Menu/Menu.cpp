#include "Menu.h"
//#include "TGFCfg.h"
#include "..\Utils\Render.h"
#include "..\SDK\Vector.h"
#include "..\SDK\ISurface.h"
#include "..\Utils\Color.h"
#include "..\Utils\GlobalVars.h"
#include "..\Utils\XorStr.h"

Menu g_Menu;

void Menu::Render()
{
	Color MainMenu(255, 0, 144,255);
	Color On(0,255,0,255);
	Color Off(255,0,0,255);
	
	std::ostringstream localPlayerStr;

	localPlayerStr << g::pLocalEntity;

	std::string tempStr = "pLocalEntity: " + localPlayerStr.str();

	const char* locPlayerCon = tempStr.c_str();

	Render::Text(300, 15, On, g::Tahoma, locPlayerCon);
	Render::Text(300, 25, MainMenu, g::Tahoma, "Hello Acel it is i radian!");

	if (GetKeyState(VK_NUMPAD0))
	{	
		if (g_Menu.Config.Esp == 0)
			g_Menu.Config.Esp = 1;
		else if (g_Menu.Config.Esp == 2)
			g_Menu.Config.Esp = 3;
		
	}
	else
	{
		if (g_Menu.Config.Esp == 1)
		{
			g_Menu.Config.Esp = 2;
		}
		else if (g_Menu.Config.Esp == 3)
		{
			g_Menu.Config.Esp = 0;
		}
	}

	std::cout << g_Menu.Config.Esp << std::endl;
	if (GetKeyState(VK_NUMPAD1))
	{

		if (g_Menu.Config.Bhop == false)
		{
			g_Menu.Config.Bhop = true;
			g_Menu.Config.AutoStrafe = true;
		}

	}
	else
	{

		if (g_Menu.Config.Bhop == true)
		{
			g_Menu.Config.Bhop = false;
			g_Menu.Config.AutoStrafe = false;

		}
	}
	if (GetKeyState(VK_NUMPAD2))
	{
		if (g_Menu.Config.Box == false)
			g_Menu.Config.Box = true;
	}
	else
	{
		if (g_Menu.Config.Box == true)
			g_Menu.Config.Box = false;
	}

	if (GetKeyState(VK_NUMPAD3))
	{
		if (g_Menu.Config.Name = 0)
			g_Menu.Config.Name = 1;

	}
	else
	{

		if (g_Menu.Config.Name = 1)
			g_Menu.Config.Name = 0;

	}

	if (GetKeyState(VK_NUMPAD6))
	{
		if (g_Menu.Config.Skeleton[0] == false && g_Menu.Config.Skeleton[1] == false)
			g_Menu.Config.Skeleton[0] = true;

		if (g_Menu.Config.Skeleton[0] == true && g_Menu.Config.Skeleton[1] == false)
			g_Menu.Config.Skeleton[1] = true;


	}
	else
	{
		if (g_Menu.Config.Skeleton[0] == true && g_Menu.Config.Skeleton[1] == true)
			g_Menu.Config.Skeleton[0] = false;
		g_Menu.Config.Skeleton[1] = false;

	}
	if (GetKeyState(VK_NUMPAD4))
	{
		if (g_Menu.Config.Chams == false)
			g_Menu.Config.Chams = true;
	}
	else
	{
		if (g_Menu.Config.Chams == true)
			g_Menu.Config.Chams = false;
	}
	if (GetKeyState(VK_NUMPAD5))
	{
		if (g_Menu.Config.LegitBacktrack == 0)
			g_Menu.Config.LegitBacktrack = 1;
		else if (g_Menu.Config.LegitBacktrack == 2)
			g_Menu.Config.LegitBacktrack = 3;

	}
	else
	{
		if (g_Menu.Config.LegitBacktrack == 1)
		{
			g_Menu.Config.LegitBacktrack = 2;
		}
		else if (g_Menu.Config.LegitBacktrack == 3)
		{
			g_Menu.Config.LegitBacktrack = 0;
			g_Menu.Config.LegitBacktrack = 0;
		}
	}
	std::cout << g_Menu.Config.LegitBacktrack << std::endl;

	Render::Text(300, 35, MainMenu, g::Tahoma, "Aim: ");
	if (Config.Aimbot == true)
		Render::Text(350, 35, On, g::Tahoma, "On");
	else
		Render::Text(350, 35, Off, g::Tahoma, "Off");

	Render::Text(300, 45, MainMenu, g::Tahoma, "AA: ");
	if (Config.Antiaim == true)
		Render::Text(350, 45, On, g::Tahoma, "On");
	else
		Render::Text(350, 45, Off, g::Tahoma, "Off");
	
	Render::Text(300, 55, MainMenu, g::Tahoma, "Bhop: ");
	if (Config.Bhop == true)
		Render::Text(350, 55, On, g::Tahoma, "On");
	else
		Render::Text(350, 55, Off, g::Tahoma, "Off");
	
	Render::Text(300, 65, MainMenu, g::Tahoma, "ESP: ");
	switch (g_Menu.Config.Esp)
	{
	default: Render::Text(350, 65, Off, g::Tahoma, "Off");
		break;
	case 1: Render::Text(350, 65, On, g::Tahoma, "On");
		break;
	case 2: Render::Text(350, 65, On, g::Tahoma, "Dead");
		break;
	}
	
	Render::Text(300, 75, MainMenu, g::Tahoma, "Chams: ");
	if (Config.Chams == true)
		Render::Text(350, 75, On, g::Tahoma, "On");
	else
		Render::Text(350, 75, Off, g::Tahoma, "Off");
	
	Render::Text(300, 85, MainMenu, g::Tahoma, "Third: ");
	if (Config.Third == true)
		Render::Text(350, 85, On, g::Tahoma, "On");
	else
		Render::Text(350, 85, Off, g::Tahoma, "Off");
	



}
